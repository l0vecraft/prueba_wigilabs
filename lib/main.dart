import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_wigilabs/core/blocs/album_cubit/album_cubit.dart';
import 'package:prueba_wigilabs/core/blocs/artist_cubit/artist_cubit.dart';
import 'package:prueba_wigilabs/core/blocs/music_cubit/music_cubit.dart';
import 'package:prueba_wigilabs/core/blocs/playlist_cubit/playlist_cubit.dart';
import 'package:prueba_wigilabs/core/blocs/user/user_cubit.dart';
import 'package:prueba_wigilabs/data/network/auth_api.dart';
import 'package:prueba_wigilabs/data/network/spotify_api.dart';
import 'package:prueba_wigilabs/data/network/user_api.dart';
import 'package:prueba_wigilabs/routes.dart';

import 'core/blocs/auth/auth_cubit.dart';
import 'core/blocs/spotify_cubit/spotify_cubit.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: Size(375, 812),
        builder: () => MultiBlocProvider(
                providers: [
                  BlocProvider(
                    create: (context) => AuthCubit(api: AuthApi()),
                  ),
                  BlocProvider(
                    create: (context) => UserCubit(api: UserApi()),
                  ),
                  BlocProvider(
                    create: (context) => SpotifyCubit(api: SpotifyApi()),
                  ),
                  BlocProvider(
                    create: (context) => PlaylistCubit(api: SpotifyApi()),
                  ),
                  BlocProvider(
                    create: (context) => MusicCubit(api: SpotifyApi()),
                  ),
                  BlocProvider(
                    create: (context) => AlbumCubit(api: SpotifyApi()),
                  ),
                  BlocProvider(
                    create: (context) => ArtistCubit(api: SpotifyApi()),
                  )
                ],
                child: MaterialApp(
                  debugShowCheckedModeBanner: false,
                  darkTheme: ThemeData.dark(),
                  onGenerateRoute: Routes.generateRoutes,
                )));
  }
}
