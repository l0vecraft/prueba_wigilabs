import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:prueba_wigilabs/data/repository/user_repository.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  final UserRepository api;
  UserCubit({required this.api}) : super(UserInitial());

  void getUserData() async {
    try {
      var res = await api.getData();
    } catch (e) {
      print(e);
    }
  }
}
