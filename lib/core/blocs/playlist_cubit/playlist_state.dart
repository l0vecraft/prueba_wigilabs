part of 'playlist_cubit.dart';

abstract class PlaylistState extends Equatable {
  const PlaylistState();

  @override
  List<Object> get props => [];
}

class PlaylistInitial extends PlaylistState {}

class PlaylistLoading extends PlaylistState {}

class PlaylistLoaded extends PlaylistState {
  final ResponsePlaylist playlist;
  PlaylistLoaded({required this.playlist});
}

class PlaylistError extends PlaylistState {
  final Failure message;
  PlaylistError({required this.message});
}
