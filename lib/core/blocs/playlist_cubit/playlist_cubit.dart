import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:prueba_wigilabs/core/model/failure.dart';
import 'package:prueba_wigilabs/core/model/playlist/playlist_model.dart';
import 'package:prueba_wigilabs/core/model/response_playlist_model.dart';
import 'package:prueba_wigilabs/data/repository/spotify_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'playlist_state.dart';

class PlaylistCubit extends Cubit<PlaylistState> {
  final SpotifyRepository api;
  PlaylistCubit({required this.api}) : super(PlaylistInitial());
  late ResponsePlaylist _playlist;

  void getPlayList(String idCategory) async {
    emit(PlaylistLoading());
    final _prefs = await SharedPreferences.getInstance();
    String? token = _prefs.getString('spoty_token');
    try {
      _playlist = await api.listPlayList(token!, idCategory);
      emit(PlaylistLoaded(playlist: _playlist));
    } on Failure catch (e) {
      emit(PlaylistError(message: e));
    }
  }
}
