part of 'spotify_cubit.dart';

abstract class SpotifyState extends Equatable {
  const SpotifyState();

  @override
  List<Object> get props => [];
}

class SpotifyInitial extends SpotifyState {}

class SpotifyAuth extends SpotifyState {
  final String token;

  SpotifyAuth({required this.token});
}

class SpotifyLoading extends SpotifyState {}

class SpotifyLoaded extends SpotifyState {
  final ResponseCategory categories;

  SpotifyLoaded({required this.categories});
}

class SpotifyError extends SpotifyState {
  final String message;

  SpotifyError({required this.message});
}
