import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:prueba_wigilabs/core/model/failure.dart';
import 'package:prueba_wigilabs/core/model/response_category_model.dart';
import 'package:prueba_wigilabs/data/repository/spotify_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'spotify_state.dart';

class SpotifyCubit extends Cubit<SpotifyState> {
  final SpotifyRepository api;
  SpotifyCubit({required this.api}) : super(SpotifyInitial());

  String _token = '';

  String get token => _token;

  void checkAuthSpotify() async {
    final _prefs = await SharedPreferences.getInstance();
    var _hasToken = _prefs.getString('spoty_token') != null;
    if (_hasToken) {
      _token = _prefs.getString('spoty_token') ?? '';

      emit(SpotifyAuth(token: _token));
    } else {
      getAuthInSpoty();
    }
  }

  Future<void> getAuthInSpoty() async {
    emit(SpotifyLoading());
    try {
      _token = await api.getAccessToken();
      emit(SpotifyAuth(token: _token));
      await getCategories();
    } on Failure catch (e) {
      emit(SpotifyError(message: e.message));
    }
  }

  Future<void> getCategories() async {
    emit(SpotifyLoading());
    try {
      final _prefs = await SharedPreferences.getInstance();
      var token = _prefs.getString('spoty_token');
      var res = await api.listCategories(token);
      emit(SpotifyAuth(token: token!));
      emit(SpotifyLoaded(categories: res));
    } on Failure catch (e) {
      emit(SpotifyError(message: e.message));
    }
  }
}
