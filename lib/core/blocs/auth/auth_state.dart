part of 'auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class AuthLoading extends AuthState {}

class AuthSignIn extends AuthState {
  final UserResponse user;

  AuthSignIn({required this.user});
}

class AuthSignOut extends AuthState {}

class AuthError extends AuthState {
  final String message;

  AuthError({required this.message});
}
