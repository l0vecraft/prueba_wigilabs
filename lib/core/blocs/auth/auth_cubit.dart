import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:prueba_wigilabs/core/model/failure.dart';
import 'package:prueba_wigilabs/core/model/user_auth_model.dart';
import 'package:prueba_wigilabs/core/model/user_response.dart';
import 'package:prueba_wigilabs/data/repository/auth_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final AuthRepository api;
  AuthCubit({required this.api}) : super(AuthInitial()) {
    checkAuth();
  }

  bool _isAuth = false;
  bool get isAuth => _isAuth;

  void checkAuth() async {
    emit(AuthLoading());
    final _prefs = await SharedPreferences.getInstance();
    _isAuth = _prefs.getString('token') != null;
    if (_isAuth) {
      var _user = UserResponse(
          email: _prefs.getString('email'), token: _prefs.getString('token'));
      emit(AuthSignIn(user: _user));
    } else {
      emit(AuthSignOut());
    }
  }

  void signInEmailAndPassword(UserAuth user) async {
    emit(AuthLoading());
    try {
      var response = await api.signInEmailPassword(user);
      if (response != null) {
        emit(AuthSignIn(user: response));
      } else {
        emit(AuthError(
            message: 'Usuario no encontrado, por favaor registrese.'));
      }
    } on Failure catch (e) {
      emit(AuthError(message: e.message));
    }
  }

  void singUpEmailAndPassword(UserAuth user) async {
    emit(AuthLoading());
    try {
      var response = await api.singUpEmailPassword(user);
      emit(AuthSignIn(user: response));
    } on Failure catch (e) {
      emit(AuthError(message: e.message));
    }
  }

  void singInGoogle() async {
    emit(AuthLoading());
    try {
      var user = await api.singInGoogle();
      emit(AuthSignIn(user: user));
    } on Failure catch (e) {
      emit(AuthError(message: e.message));
    }
  }

  void singInFacebook() async {
    emit(AuthLoading());
    try {
      var user = await api.singInFacebook();
      emit(AuthSignIn(user: user));
    } on Failure catch (e) {
      emit(AuthError(message: e.message));
    }
  }

  Future<bool> _isSigninGoogle() async {
    var _isSignin = false;

    try {
      _isSignin = await api.isSignIn();
    } on Failure catch (e) {
      emit(AuthError(message: e.message));
    }
    return _isSignin;
  }

  void signOut() async {
    emit(AuthLoading());
    final _prefs = await SharedPreferences.getInstance();
    var _isSigned = await _isSigninGoogle();
    if (_isSigned) {
      await api.singOutGoogle();
      _prefs.clear();
    } else {
      await api.signOut();
      _prefs.clear();
    }
    emit(AuthSignOut());
  }
}
