part of 'artist_cubit.dart';

abstract class ArtistState extends Equatable {
  const ArtistState();

  @override
  List<Object> get props => [];
}

class ArtistInitial extends ArtistState {}

class ArtistLoading extends ArtistState {}

class ArtistLoaded extends ArtistState {
  final ArtistDetail artist;
  final ArtistAlbum albums;
  final TopTracks topTracks;
  ArtistLoaded(
      {required this.artist, required this.albums, required this.topTracks});
}

class ArtistError extends ArtistState {
  final Failure message;
  ArtistError({
    required this.message,
  });
}
