import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:prueba_wigilabs/core/model/album_model.dart';
import 'package:prueba_wigilabs/core/model/artist/artist_album_model.dart';
import 'package:prueba_wigilabs/core/model/artist/artist_detail_model.dart';
import 'package:prueba_wigilabs/core/model/failure.dart';
import 'package:prueba_wigilabs/core/model/top_tracks_model.dart';
import 'package:prueba_wigilabs/data/repository/spotify_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'artist_state.dart';

class ArtistCubit extends Cubit<ArtistState> {
  final SpotifyRepository api;
  ArtistCubit({required this.api}) : super(ArtistInitial());

  void getArtistDetail(String? idArtist) async {
    emit(ArtistLoading());
    try {
      final _prefs = await SharedPreferences.getInstance();
      String? token = _prefs.getString('spoty_token');
      var res = await api.getArtistDetail(token!, idArtist);
      var resAlbums = await api.getArtistAlbums(token, idArtist);
      var topTracks = await api.getTopSongsArtist(token, idArtist);
      emit(ArtistLoaded(artist: res, albums: resAlbums, topTracks: topTracks));
    } on Failure catch (e) {
      emit(ArtistError(message: e));
    }
  }
}
