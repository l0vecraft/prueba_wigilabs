part of 'album_cubit.dart';

abstract class AlbumState extends Equatable {
  const AlbumState();

  @override
  List<Object> get props => [];
}

class AlbumInitial extends AlbumState {}

class AlbumLoading extends AlbumState {}

class AlbumLoaded extends AlbumState {
  final AlbumModel albumModel;
  AlbumLoaded({required this.albumModel});
}

class AlbumError extends AlbumState {
  final Failure message;
  AlbumError({required this.message});
}
