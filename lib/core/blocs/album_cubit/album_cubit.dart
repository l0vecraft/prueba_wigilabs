import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:prueba_wigilabs/core/model/album_model.dart';
import 'package:prueba_wigilabs/core/model/failure.dart';
import 'package:prueba_wigilabs/data/repository/spotify_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'album_state.dart';

class AlbumCubit extends Cubit<AlbumState> {
  final SpotifyRepository api;
  AlbumCubit({required this.api}) : super(AlbumInitial());

  void getAlbumTracks(String? idAlbum) async {
    emit(AlbumLoading());
    try {
      final _prefs = await SharedPreferences.getInstance();
      String? token = _prefs.getString('spoty_token');
      var res = await api.getAlbumOfArtist(token!, idAlbum);
      emit(AlbumLoaded(albumModel: res));
    } on Failure catch (e) {
      emit(AlbumError(message: e));
    }
  }
}
