part of 'music_cubit.dart';

abstract class MusicState extends Equatable {
  const MusicState();

  @override
  List<Object> get props => [];
}

class MusicInitial extends MusicState {}

class MusicLoading extends MusicState {}

class MusicLoaded extends MusicState {
  final MusicListModel listMusic;
  final TrackMusic? trackSelected;
  MusicLoaded({required this.listMusic, this.trackSelected});
}

class MusicError extends MusicState {
  final Failure message;
  MusicError({
    required this.message,
  });
}
