import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:prueba_wigilabs/core/model/failure.dart';
import 'package:prueba_wigilabs/core/model/playlist/music_list_model.dart';
import 'package:prueba_wigilabs/data/repository/spotify_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'music_state.dart';

class MusicCubit extends Cubit<MusicState> {
  final SpotifyRepository api;
  MusicCubit({required this.api}) : super(MusicInitial());

  TrackMusic _selectedTrack = TrackMusic();
  MusicListModel? _musicListModel;

  TrackMusic get selectedTrack => _selectedTrack;

  set selectedTrack(TrackMusic value) {
    _selectedTrack = value;
  }

  void getMusicList(String? idPlaylist) async {
    emit(MusicLoading());
    final _prefs = await SharedPreferences.getInstance();
    String? token = _prefs.getString('spoty_token');
    try {
      _musicListModel = await api.getSongsOfPlayList(token!, idPlaylist);
      emit(MusicLoaded(listMusic: _musicListModel!));
    } on Failure catch (e) {
      emit(MusicError(message: e));
    }
  }

  void getTrack(TrackMusic? track) async {
    emit(MusicLoading());
    final _prefs = await SharedPreferences.getInstance();
    String? token = _prefs.getString('spoty_token');
    _selectedTrack = track!;
    try {
      emit(MusicLoaded(
          listMusic: _musicListModel!, trackSelected: _selectedTrack));
    } on Failure catch (e) {
      emit(MusicError(message: e));
    }
  }
}
