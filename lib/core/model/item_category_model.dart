import 'dart:convert';

import 'category_icon_model.dart';

class ItemCategory {
  String? href;
  List<CategoryIcon>? icons;
  String id;
  String? name;

  ItemCategory({this.href, this.icons, required this.id, this.name});

  Map<String, dynamic> toMap() {
    return {
      'href': href,
      'icons': icons?.map((x) => x.toMap()).toList(),
      'id': id,
      'name': name,
    };
  }

  factory ItemCategory.fromMap(Map<String, dynamic> map) {
    return ItemCategory(
      href: map['href'],
      icons: List<CategoryIcon>.from(
          map['icons']?.map((x) => CategoryIcon.fromMap(x))),
      id: map['id'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ItemCategory.fromJson(String source) =>
      ItemCategory.fromMap(json.decode(source));
}
