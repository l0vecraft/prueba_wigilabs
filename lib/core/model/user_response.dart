import 'package:equatable/equatable.dart';

class UserResponse implements Equatable {
  final String? email;
  final String? token;

  UserResponse({this.email, this.token});
  @override
  List<Object> get props => [];

  @override
  // TODO: implement stringify
  bool get stringify => throw UnimplementedError();
}
