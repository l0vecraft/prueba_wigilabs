import 'dart:convert';

class Tracks {
  String? href;
  int? total;

  Tracks({this.href, this.total});

  Map<String, dynamic> toMap() {
    return {
      'href': href,
      'total': total,
    };
  }

  factory Tracks.fromMap(Map<String, dynamic> map) {
    return Tracks(
      href: map['href'],
      total: map['total'] ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory Tracks.fromJson(String source) => Tracks.fromMap(json.decode(source));
}
