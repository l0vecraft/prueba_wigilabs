import 'dart:convert';

import 'package:prueba_wigilabs/core/model/playlist/item_playlist_model.dart';

class PlaylistModel {
  String href;
  List<ItemsPlaylist> items;
  int limit;
  String next;
  int offset;
  String previous;
  int total;
  PlaylistModel({
    required this.href,
    required this.items,
    required this.limit,
    required this.next,
    required this.offset,
    required this.previous,
    required this.total,
  });

  Map<String, dynamic> toMap() {
    return {
      'href': href,
      'items': items.map((x) => x.toMap()).toList(),
      'limit': limit,
      'next': next,
      'offset': offset,
      'previous': previous,
      'total': total,
    };
  }

  factory PlaylistModel.fromMap(Map<String, dynamic> map) {
    return PlaylistModel(
      href: map['href'],
      items: List<ItemsPlaylist>.from(
          map['items']?.map((x) => ItemsPlaylist.fromMap(x))),
      limit: map['limit'],
      next: map['next'] ?? '',
      offset: map['offset'],
      previous: map['previous'] ?? '',
      total: map['total'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PlaylistModel.fromJson(String source) =>
      PlaylistModel.fromMap(json.decode(source));
}
