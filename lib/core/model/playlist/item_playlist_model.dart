import 'dart:convert';

import 'external_links.dart';
import 'image_playlist_model.dart';
import 'owner_playlist_model.dart';
import 'track_playlist_model.dart';

class ItemsPlaylist {
  bool? collaborative;
  String? description;

  String? href;
  String? id;
  List<ImagesPlayList>? images;
  String? name;
  Owner? owner;
  Null primaryColor;
  Null public;
  String? snapshotId;
  Tracks? tracks;
  String? type;
  String? uri;

  ItemsPlaylist(
      {this.collaborative,
      this.description,
      this.href,
      this.id,
      this.images,
      this.name,
      this.owner,
      this.primaryColor,
      this.public,
      this.snapshotId,
      this.tracks,
      this.type,
      this.uri});

  Map<String, dynamic> toMap() {
    return {
      'collaborative': collaborative,
      'description': description,
      'href': href,
      'id': id,
      'images': images?.map((x) => x.toMap()).toList(),
      'name': name,
      'owner': owner?.toMap(),
      'primaryColor': primaryColor,
      'public': public,
      'snapshotId': snapshotId,
      'tracks': tracks?.toMap(),
      'type': type,
      'uri': uri,
    };
  }

  factory ItemsPlaylist.fromMap(Map<String, dynamic> map) {
    return ItemsPlaylist(
      collaborative: map['collaborative'],
      description: map['description'],
      href: map['href'],
      id: map['id'],
      images: List<ImagesPlayList>.from(
          map['images']?.map((x) => ImagesPlayList.fromMap(x))),
      name: map['name'],
      owner: Owner.fromMap(map['owner']),
      primaryColor: map['primaryColor'],
      public: map['public'],
      snapshotId: map['snapshotId'],
      tracks: Tracks.fromMap(map['tracks']),
      type: map['type'],
      uri: map['uri'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ItemsPlaylist.fromJson(String source) =>
      ItemsPlaylist.fromMap(json.decode(source));
}
