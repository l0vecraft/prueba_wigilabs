import 'dart:convert';
import 'package:prueba_wigilabs/core/model/playlist/image_playlist_model.dart';
import 'package:prueba_wigilabs/core/model/playlist/owner_playlist_model.dart';

class MusicListModel {
  bool collaborative;
  String description;
  Followers followers;
  String href;
  String id;
  List<ImagesPlayList> images;
  String name;
  Owner owner;
  String primaryColor;
  bool public;
  String snapshotId;
  MusicTracks tracks;
  String type;
  String uri;
  MusicListModel({
    required this.collaborative,
    required this.description,
    required this.followers,
    required this.href,
    required this.id,
    required this.images,
    required this.name,
    required this.owner,
    required this.primaryColor,
    required this.public,
    required this.snapshotId,
    required this.tracks,
    required this.type,
    required this.uri,
  });

  Map<String, dynamic> toMap() {
    return {
      'collaborative': collaborative,
      'description': description,
      'followers': followers.toMap(),
      'href': href,
      'id': id,
      'images': images.map((x) => x.toMap()).toList(),
      'name': name,
      'owner': owner.toMap(),
      'primaryColor': primaryColor,
      'public': public,
      'snapshotId': snapshotId,
      'tracks': tracks.toMap(),
      'type': type,
      'uri': uri,
    };
  }

  factory MusicListModel.fromMap(Map<String, dynamic> map) {
    return MusicListModel(
      collaborative: map['collaborative'],
      description: map['description'] ?? '',
      followers: Followers.fromMap(map['followers']),
      href: map['href'] ?? '',
      id: map['id'],
      images: List<ImagesPlayList>.from(
          map['images']?.map((x) => ImagesPlayList.fromMap(x))),
      name: map['name'] ?? '',
      owner: Owner.fromMap(map['owner']),
      primaryColor: map['primaryColor'] ?? '',
      public: map['public'],
      snapshotId: map['snapshotId'] ?? '',
      tracks: MusicTracks.fromMap(map['tracks']),
      type: map['type'],
      uri: map['uri'],
    );
  }

  String toJson() => json.encode(toMap());

  factory MusicListModel.fromJson(String source) =>
      MusicListModel.fromMap(json.decode(source));
}

class Followers {
  String href;
  int total;

  Followers({required this.href, required this.total});

  Map<String, dynamic> toMap() {
    return {
      'href': href,
      'total': total,
    };
  }

  factory Followers.fromMap(Map<String, dynamic> map) {
    return Followers(
      href: map['href'] ?? '',
      total: map['total'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Followers.fromJson(String source) =>
      Followers.fromMap(json.decode(source));
}

class MusicTracks {
  String href;
  List<ItemsMusic> items;
  int limit;
  String? next;
  int offset;
  String? previous;
  int total;
  MusicTracks({
    required this.href,
    required this.items,
    required this.limit,
    required this.next,
    required this.offset,
    required this.previous,
    required this.total,
  });

  Map<String, dynamic> toMap() {
    return {
      'href': href,
      'items': items.map((x) => x.toMap()).toList(),
      'limit': limit,
      'next': next,
      'offset': offset,
      'previous': previous,
      'total': total,
    };
  }

  factory MusicTracks.fromMap(Map<String, dynamic> map) {
    return MusicTracks(
      href: map['href'],
      items: List<ItemsMusic>.from(
          map['items']?.map((x) => ItemsMusic.fromMap(x))),
      limit: map['limit'],
      next: map['next'],
      offset: map['offset'],
      previous: map['previous'],
      total: map['total'],
    );
  }

  String toJson() => json.encode(toMap());

  factory MusicTracks.fromJson(String source) =>
      MusicTracks.fromMap(json.decode(source));
}

class ItemsMusic {
  String addedAt;
  // AddedBy? addedBy;
  bool isLocal;
  String primaryColor;
  TrackMusic track;
  VideoThumbnail videoThumbnail;
  ItemsMusic({
    required this.addedAt,
    // required this.addedBy,
    required this.isLocal,
    required this.primaryColor,
    required this.track,
    required this.videoThumbnail,
  });

  Map<String, dynamic> toMap() {
    return {
      'addedAt': addedAt,
      // 'addedBy': addedBy?.toMap(),
      'isLocal': isLocal,
      'primaryColor': primaryColor,
      'track': track.toMap(),
      'videoThumbnail': videoThumbnail.toMap(),
    };
  }

  factory ItemsMusic.fromMap(Map<String, dynamic> map) {
    return ItemsMusic(
      addedAt: map['addedAt'] ?? '',
      // addedBy: AddedBy.fromMap(map['addedBy']),
      isLocal: map['isLocal'] ?? false,
      primaryColor: map['primaryColor'] ?? '',
      track: TrackMusic.fromMap(map['track']),
      videoThumbnail: map['videoThumbnail'] == null
          ? VideoThumbnail(url: '')
          : VideoThumbnail.fromMap(map['videoThumbnail']),
    );
  }

  String toJson() => json.encode(toMap());

  factory ItemsMusic.fromJson(String source) =>
      ItemsMusic.fromMap(json.decode(source));
}

class AddedBy {
  String href;
  String id;
  String type;
  String uri;
  AddedBy({
    required this.href,
    required this.id,
    required this.type,
    required this.uri,
  });

  Map<String, dynamic> toMap() {
    return {
      'href': href,
      'id': id,
      'type': type,
      'uri': uri,
    };
  }

  factory AddedBy.fromMap(Map<String, dynamic> map) {
    return AddedBy(
      href: map['href'],
      id: map['id'],
      type: map['type'],
      uri: map['uri'],
    );
  }

  String toJson() => json.encode(toMap());

  factory AddedBy.fromJson(String source) =>
      AddedBy.fromMap(json.decode(source));
}

class TrackMusic {
  Album? album;
  List<Artists>? artists;
  int? discNumber;
  int? durationMs;
  bool? episode;
  bool? explicit;
  String? href;
  String? id;
  bool? isLocal;
  String? name;
  int? popularity;
  String? previewUrl;
  bool? track;
  int? trackNumber;
  String? type;
  String? uri;
  TrackMusic({
    this.album,
    this.artists,
    this.discNumber,
    this.durationMs,
    this.episode,
    this.explicit,
    this.href,
    this.id,
    this.isLocal,
    this.name,
    this.popularity,
    this.previewUrl,
    this.track,
    this.trackNumber,
    this.type,
    this.uri,
  });

  Map<String, dynamic> toMap() {
    return {
      'album': album?.toMap(),
      'artists': artists?.map((x) => x.toMap()).toList(),
      'discNumber': discNumber,
      'durationMs': durationMs,
      'episode': episode,
      'explicit': explicit,
      'href': href,
      'id': id,
      'isLocal': isLocal,
      'name': name,
      'popularity': popularity,
      'previewUrl': previewUrl,
      'track': track,
      'trackNumber': trackNumber,
      'type': type,
      'uri': uri,
    };
  }

  factory TrackMusic.fromMap(Map<String, dynamic> map) {
    return TrackMusic(
      album: Album.fromMap(map['album']),
      artists:
          List<Artists>.from(map['artists']?.map((x) => Artists.fromMap(x))),
      discNumber: map['discNumber'] ?? 0,
      durationMs: map['durationMs'] ?? 0,
      episode: map['episode'],
      explicit: map['explicit'],
      href: map['href'],
      id: map['id'],
      isLocal: map['isLocal'] ?? false,
      name: map['name'],
      popularity: map['popularity'],
      previewUrl: map['previewUrl'] ?? '',
      track: map['track'],
      trackNumber: map['trackNumber'] ?? 0,
      type: map['type'],
      uri: map['uri'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TrackMusic.fromJson(String source) =>
      TrackMusic.fromMap(json.decode(source));
}

class Album {
  String albumType;
  List<Artists> artists;
  String href;
  String id;
  List<ImagesPlayList> images;
  String name;
  String releaseDate;
  String releaseDatePrecision;
  int totalTracks;
  String type;
  String uri;
  Album({
    required this.albumType,
    required this.artists,
    required this.href,
    required this.id,
    required this.images,
    required this.name,
    required this.releaseDate,
    required this.releaseDatePrecision,
    required this.totalTracks,
    required this.type,
    required this.uri,
  });

  Map<String, dynamic> toMap() {
    return {
      'albumType': albumType,
      'artists': artists.map((x) => x.toMap()).toList(),
      'href': href,
      'id': id,
      'images': images.map((x) => x.toMap()).toList(),
      'name': name,
      'releaseDate': releaseDate,
      'releaseDatePrecision': releaseDatePrecision,
      'totalTracks': totalTracks,
      'type': type,
      'uri': uri,
    };
  }

  factory Album.fromMap(Map<String, dynamic> map) {
    return Album(
      albumType: map['albumType'] ?? '',
      artists:
          List<Artists>.from(map['artists'].map((x) => Artists.fromMap(x))),
      href: map['href'],
      id: map['id'],
      images: List<ImagesPlayList>.from(
          map['images']?.map((x) => ImagesPlayList.fromMap(x))),
      name: map['name'] ?? '',
      releaseDate: map['releaseDate'] ?? '',
      releaseDatePrecision: map['releaseDatePrecision'] ?? '',
      totalTracks: map['totalTracks'] ?? 0,
      type: map['type'],
      uri: map['uri'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory Album.fromJson(String source) => Album.fromMap(json.decode(source));
}

class Artists {
  String href;
  String id;
  String name;
  String type;
  String uri;
  Artists({
    required this.href,
    required this.id,
    required this.name,
    required this.type,
    required this.uri,
  });

  Map<String, dynamic> toMap() {
    return {
      'href': href,
      'id': id,
      'name': name,
      'type': type,
      'uri': uri,
    };
  }

  factory Artists.fromMap(Map<String, dynamic> map) {
    return Artists(
      href: map['href'],
      id: map['id'],
      name: map['name'],
      type: map['type'],
      uri: map['uri'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Artists.fromJson(String source) =>
      Artists.fromMap(json.decode(source));
}

class ExternalIds {
  String isrc;
  ExternalIds({
    required this.isrc,
  });

  Map<String, dynamic> toMap() {
    return {
      'isrc': isrc,
    };
  }

  factory ExternalIds.fromMap(Map<String, dynamic> map) {
    return ExternalIds(
      isrc: map['isrc'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ExternalIds.fromJson(String source) =>
      ExternalIds.fromMap(json.decode(source));
}

class VideoThumbnail {
  String url;
  VideoThumbnail({
    required this.url,
  });

  Map<String, dynamic> toMap() {
    return {
      'url': url,
    };
  }

  factory VideoThumbnail.fromMap(Map<String, dynamic> map) {
    return VideoThumbnail(
      url: map['url'],
    );
  }

  String toJson() => json.encode(toMap());

  factory VideoThumbnail.fromJson(String source) =>
      VideoThumbnail.fromMap(json.decode(source));
}
