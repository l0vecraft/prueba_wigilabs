import 'dart:convert';

class ExternalUrls {
  String? spotify;

  ExternalUrls({this.spotify});

  Map<String, dynamic> toMap() {
    return {
      'spotify': spotify,
    };
  }

  factory ExternalUrls.fromMap(Map<String, dynamic> map) {
    return ExternalUrls(
      spotify: map['spotify'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ExternalUrls.fromJson(String source) =>
      ExternalUrls.fromMap(json.decode(source));
}
