import 'dart:convert';

class Owner {
  String? displayName;

  String? href;
  String? id;
  String? type;
  String? uri;

  Owner({this.displayName, this.href, this.id, this.type, this.uri});

  Map<String, dynamic> toMap() {
    return {
      'displayName': displayName,
      'href': href,
      'id': id,
      'type': type,
      'uri': uri,
    };
  }

  factory Owner.fromMap(Map<String, dynamic> map) {
    return Owner(
      displayName: map['displayName'],
      href: map['href'],
      id: map['id'],
      type: map['type'],
      uri: map['uri'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Owner.fromJson(String source) => Owner.fromMap(json.decode(source));
}
