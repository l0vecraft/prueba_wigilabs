import 'dart:convert';

class ImagesPlayList {
  int height;
  String url;
  int width;
  ImagesPlayList({
    required this.height,
    required this.url,
    required this.width,
  });

  Map<String, dynamic> toMap() {
    return {
      'height': height,
      'url': url,
      'width': width,
    };
  }

  factory ImagesPlayList.fromMap(Map<String, dynamic> map) {
    return ImagesPlayList(
      height: map['height'] ?? 0,
      url: map['url'],
      width: map['width'] ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory ImagesPlayList.fromJson(String source) =>
      ImagesPlayList.fromMap(json.decode(source));
}
