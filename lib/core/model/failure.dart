import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Failure implements Exception {
  final String message;

  Failure({required this.message});
}
