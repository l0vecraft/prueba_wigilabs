import 'dart:convert';

import 'playlist/image_playlist_model.dart';
import 'playlist/music_list_model.dart';

class AlbumModel {
  String albumType;
  List<Artists> artists;
  List<Copyrights> copyrights;
  List genres;
  String href;
  String id;
  List<ImagesPlayList> images;
  String label;
  String name;
  int popularity;
  String releaseDate;
  String releaseDatePrecision;
  int totalTracks;
  AlbumTracks tracks;
  String type;
  String uri;
  AlbumModel({
    required this.albumType,
    required this.artists,
    required this.copyrights,
    required this.genres,
    required this.href,
    required this.id,
    required this.images,
    required this.label,
    required this.name,
    required this.popularity,
    required this.releaseDate,
    required this.releaseDatePrecision,
    required this.totalTracks,
    required this.tracks,
    required this.type,
    required this.uri,
  });

  Map<String, dynamic> toMap() {
    return {
      'albumType': albumType,
      'artists': artists.map((x) => x.toMap()).toList(),
      'copyrights': copyrights.map((x) => x.toMap()).toList(),
      'genres': genres,
      'href': href,
      'id': id,
      'images': images.map((x) => x.toMap()).toList(),
      'label': label,
      'name': name,
      'popularity': popularity,
      'releaseDate': releaseDate,
      'releaseDatePrecision': releaseDatePrecision,
      'totalTracks': totalTracks,
      'tracks': tracks.toMap(),
      'type': type,
      'uri': uri,
    };
  }

  factory AlbumModel.fromMap(Map<String, dynamic> map) {
    return AlbumModel(
      albumType: map['albumType'] ?? '',
      artists: map['artists'] == null
          ? []
          : List<Artists>.from(map['artists']?.map((x) => Artists.fromMap(x))),
      copyrights: map['copyrights'] == null
          ? []
          : List<Copyrights>.from(
              map['copyrights']?.map((x) => Copyrights.fromMap(x))),
      genres: map['genres'] == null ? [] : List.from(map['genres']),
      href: map['href'] ?? '',
      id: map['id'],
      images: List<ImagesPlayList>.from(
          map['images']?.map((x) => ImagesPlayList.fromMap(x))),
      label: map['label'] ?? '',
      name: map['name'],
      popularity: map['popularity'] ?? 0,
      releaseDate: map['releaseDate'] ?? '',
      releaseDatePrecision: map['releaseDatePrecision'] ?? '',
      totalTracks: map['totalTracks'] ?? 0,
      tracks: AlbumTracks.fromMap(map['tracks']),
      type: map['type'] ?? '',
      uri: map['uri'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory AlbumModel.fromJson(String source) =>
      AlbumModel.fromMap(json.decode(source));
}

class Copyrights {
  String text;
  String type;
  Copyrights({
    required this.text,
    required this.type,
  });

  Map<String, dynamic> toMap() {
    return {
      'text': text,
      'type': type,
    };
  }

  factory Copyrights.fromMap(Map<String, dynamic> map) {
    return Copyrights(
      text: map['text'],
      type: map['type'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Copyrights.fromJson(String source) =>
      Copyrights.fromMap(json.decode(source));
}

class AlbumTracks {
  String href;
  List<AlbumItems> items;
  int limit;
  String next;
  int offset;
  String previous;
  int total;
  AlbumTracks({
    required this.href,
    required this.items,
    required this.limit,
    required this.next,
    required this.offset,
    required this.previous,
    required this.total,
  });

  Map<String, dynamic> toMap() {
    return {
      'href': href,
      'items': items.map((x) => x.toMap()).toList(),
      'limit': limit,
      'next': next,
      'offset': offset,
      'previous': previous,
      'total': total,
    };
  }

  factory AlbumTracks.fromMap(Map<String, dynamic> map) {
    return AlbumTracks(
      href: map['href'] ?? '',
      items: List<AlbumItems>.from(
          map['items']?.map((x) => AlbumItems.fromMap(x))),
      limit: map['limit'] ?? 0,
      next: map['next'] ?? '',
      offset: map['offset'] ?? 0,
      previous: map['previous'] ?? '',
      total: map['total'] ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory AlbumTracks.fromJson(String source) =>
      AlbumTracks.fromMap(json.decode(source));
}

class AlbumItems {
  List<Artists> artists;
  int discNumber;
  int durationMs;
  bool explicit;
  String href;
  String id;
  bool isLocal;
  String name;
  String previewUrl;
  int trackNumber;
  String type;
  String uri;
  AlbumItems({
    required this.artists,
    required this.discNumber,
    required this.durationMs,
    required this.explicit,
    required this.href,
    required this.id,
    required this.isLocal,
    required this.name,
    required this.previewUrl,
    required this.trackNumber,
    required this.type,
    required this.uri,
  });

  Map<String, dynamic> toMap() {
    return {
      'artists': artists.map((x) => x.toMap()).toList(),
      'discNumber': discNumber,
      'durationMs': durationMs,
      'explicit': explicit,
      'href': href,
      'id': id,
      'isLocal': isLocal,
      'name': name,
      'previewUrl': previewUrl,
      'trackNumber': trackNumber,
      'type': type,
      'uri': uri,
    };
  }

  factory AlbumItems.fromMap(Map<String, dynamic> map) {
    return AlbumItems(
      artists:
          List<Artists>.from(map['artists']?.map((x) => Artists.fromMap(x))),
      discNumber: map['discNumber'] ?? 0,
      durationMs: map['durationMs'] ?? 0,
      explicit: map['explicit'] ?? false,
      href: map['href'] ?? '',
      id: map['id'],
      isLocal: map['isLocal'] ?? false,
      name: map['name'],
      previewUrl: map['previewUrl'] ?? '',
      trackNumber: map['trackNumber'] ?? 0,
      type: map['type'] ?? '',
      uri: map['uri'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory AlbumItems.fromJson(String source) =>
      AlbumItems.fromMap(json.decode(source));
}
