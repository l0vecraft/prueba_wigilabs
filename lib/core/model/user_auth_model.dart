import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class UserAuth implements Equatable {
  final String email;
  final String password;

  UserAuth({required this.email, required this.password});
  @override
  List<Object> get props => [this.email, this.password];

  @override
  // TODO: implement stringify
  bool get stringify => throw UnimplementedError();
}
