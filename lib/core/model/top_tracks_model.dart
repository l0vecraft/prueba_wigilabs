import 'dart:convert';

import 'playlist/music_list_model.dart';

class TopTracks {
  List<TopTracksItem> tracks;

  TopTracks({required this.tracks});

  Map<String, dynamic> toMap() {
    return {
      'tracks': tracks.map((x) => x.toMap()).toList(),
    };
  }

  factory TopTracks.fromMap(Map<String, dynamic> map) {
    return TopTracks(
      tracks: List<TopTracksItem>.from(
          map['tracks']?.map((x) => TopTracksItem.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory TopTracks.fromJson(String source) =>
      TopTracks.fromMap(json.decode(source));
}

class TopTracksItem {
  Album album;
  List<Artists> artists;
  int discNumber;
  int durationMs;
  bool explicit;
  String href;
  String id;
  bool isLocal;
  bool isPlayable;
  String name;
  int popularity;
  String previewUrl;
  int trackNumber;
  String type;
  String uri;
  TopTracksItem({
    required this.album,
    required this.artists,
    required this.discNumber,
    required this.durationMs,
    required this.explicit,
    required this.href,
    required this.id,
    required this.isLocal,
    required this.isPlayable,
    required this.name,
    required this.popularity,
    required this.previewUrl,
    required this.trackNumber,
    required this.type,
    required this.uri,
  });

  Map<String, dynamic> toMap() {
    return {
      'album': album.toMap(),
      'artists': artists.map((x) => x.toMap()).toList(),
      'discNumber': discNumber,
      'durationMs': durationMs,
      'explicit': explicit,
      'href': href,
      'id': id,
      'isLocal': isLocal,
      'isPlayable': isPlayable,
      'name': name,
      'popularity': popularity,
      'previewUrl': previewUrl,
      'trackNumber': trackNumber,
      'type': type,
      'uri': uri,
    };
  }

  factory TopTracksItem.fromMap(Map<String, dynamic> map) {
    return TopTracksItem(
      album: Album.fromMap(map['album']),
      artists:
          List<Artists>.from(map['artists']?.map((x) => Artists.fromMap(x))),
      discNumber: map['discNumber'] ?? 0,
      durationMs: map['durationMs'] ?? 0,
      explicit: map['explicit'] ?? false,
      href: map['href'] ?? '',
      id: map['id'],
      isLocal: map['isLocal'] ?? false,
      isPlayable: map['isPlayable'] ?? false,
      name: map['name'] ?? '',
      popularity: map['popularity'] ?? 0,
      previewUrl: map['previewUrl'] ?? '',
      trackNumber: map['trackNumber'] ?? 0,
      type: map['type'] ?? '',
      uri: map['uri'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory TopTracksItem.fromJson(String source) =>
      TopTracksItem.fromMap(json.decode(source));
}
