import 'dart:convert';

import 'item_category_model.dart';

class Categories {
  String? href;
  List<ItemCategory>? items;
  int? limit;
  String? next;
  int? offset;
  String? previous;
  int? total;

  Categories(
      {this.href,
      this.items,
      this.limit,
      this.next,
      this.offset,
      this.previous,
      this.total});

  Map<String, dynamic> toMap() {
    return {
      'href': href,
      'items': items?.map((x) => x.toMap()).toList(),
      'limit': limit,
      'next': next,
      '?offset': offset,
      'previous': previous,
      '?total': total,
    };
  }

  factory Categories.fromMap(Map<String, dynamic> map) {
    return Categories(
      href: map['href'],
      items: List<ItemCategory>.from(
          map['items']?.map((x) => ItemCategory.fromMap(x))),
      limit: map['limit'],
      next: map['next'] ?? '',
      offset: map['offset'],
      previous: map['previous'],
      total: map['total'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Categories.fromJson(String source) =>
      Categories.fromMap(json.decode(source));
}
