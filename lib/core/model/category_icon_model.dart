import 'dart:convert';

class CategoryIcon {
  int height;
  String url;
  int width;

  CategoryIcon({required this.height, required this.url, required this.width});

  Map<String, dynamic> toMap() {
    return {
      'height': height,
      'url': url,
      'width': width,
    };
  }

  factory CategoryIcon.fromMap(Map<String, dynamic> map) {
    return CategoryIcon(
      height: map['height'] ?? 0,
      url: map['url'] ?? '',
      width: map['width'] ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory CategoryIcon.fromJson(String source) =>
      CategoryIcon.fromMap(json.decode(source));
}
