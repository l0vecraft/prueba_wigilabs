import 'dart:convert';

import 'package:prueba_wigilabs/core/model/playlist/image_playlist_model.dart';
import 'package:prueba_wigilabs/core/model/playlist/music_list_model.dart';

class ArtistAlbum {
  String href;
  List<ArtistAlbumItems> items;
  int limit;
  String next;
  int offset;
  String previous;
  int total;
  ArtistAlbum({
    required this.href,
    required this.items,
    required this.limit,
    required this.next,
    required this.offset,
    required this.previous,
    required this.total,
  });

  Map<String, dynamic> toMap() {
    return {
      'href': href,
      'items': items.map((x) => x.toMap()).toList(),
      'limit': limit,
      'next': next,
      'offset': offset,
      'previous': previous,
      'total': total,
    };
  }

  factory ArtistAlbum.fromMap(Map<String, dynamic> map) {
    return ArtistAlbum(
      href: map['href'] ?? '',
      items: List<ArtistAlbumItems>.from(
          map['items']?.map((x) => ArtistAlbumItems.fromMap(x))),
      limit: map['limit'] ?? 0,
      next: map['next'] ?? '',
      offset: map['offset'] ?? 0,
      previous: map['previous'] ?? '',
      total: map['total'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ArtistAlbum.fromJson(String source) =>
      ArtistAlbum.fromMap(json.decode(source));
}

class ArtistAlbumItems {
  String albumGroup;
  String albumType;
  List<Artists> artists;

  String href;
  String id;
  List<ImagesPlayList> images;
  String name;
  String releaseDate;
  String releaseDatePrecision;
  int totalTracks;
  String type;
  String uri;
  ArtistAlbumItems({
    required this.albumGroup,
    required this.albumType,
    required this.artists,
    required this.href,
    required this.id,
    required this.images,
    required this.name,
    required this.releaseDate,
    required this.releaseDatePrecision,
    required this.totalTracks,
    required this.type,
    required this.uri,
  });

  Map<String, dynamic> toMap() {
    return {
      'albumGroup': albumGroup,
      'albumType': albumType,
      'artists': artists.map((x) => x.toMap()).toList(),
      'href': href,
      'id': id,
      'images': images.map((x) => x.toMap()).toList(),
      'name': name,
      'releaseDate': releaseDate,
      'releaseDatePrecision': releaseDatePrecision,
      'totalTracks': totalTracks,
      'type': type,
      'uri': uri,
    };
  }

  factory ArtistAlbumItems.fromMap(Map<String, dynamic> map) {
    return ArtistAlbumItems(
      albumGroup: map['albumGroup'] ?? '',
      albumType: map['albumType'] ?? '',
      artists:
          List<Artists>.from(map['artists']?.map((x) => Artists.fromMap(x))),
      href: map['href'] ?? '',
      id: map['id'],
      images: List<ImagesPlayList>.from(
          map['images']?.map((x) => ImagesPlayList.fromMap(x))),
      name: map['name'],
      releaseDate: map['releaseDate'] ?? '',
      releaseDatePrecision: map['releaseDatePrecision'] ?? '',
      totalTracks: map['totalTracks'] ?? 0,
      type: map['type'] ?? '',
      uri: map['uri'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory ArtistAlbumItems.fromJson(String source) =>
      ArtistAlbumItems.fromMap(json.decode(source));
}
