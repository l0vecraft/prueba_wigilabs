import 'dart:convert';

import 'package:prueba_wigilabs/core/model/playlist/image_playlist_model.dart';

class ArtistDetail {
  ArtistFollowers followers;
  List<String> genres;
  String href;
  String id;
  List<ImagesPlayList> images;
  String name;
  int popularity;
  String type;
  String uri;
  ArtistDetail({
    required this.followers,
    required this.genres,
    required this.href,
    required this.id,
    required this.images,
    required this.name,
    required this.popularity,
    required this.type,
    required this.uri,
  });

  Map<String, dynamic> toMap() {
    return {
      'followers': followers.toMap(),
      'genres': genres,
      'href': href,
      'id': id,
      'images': images.map((x) => x.toMap()).toList(),
      'name': name,
      'popularity': popularity,
      'type': type,
      'uri': uri,
    };
  }

  factory ArtistDetail.fromMap(Map<String, dynamic> map) {
    return ArtistDetail(
      followers: ArtistFollowers.fromMap(map['followers']),
      genres: List<String>.from(map['genres']),
      href: map['href'],
      id: map['id'],
      images: List<ImagesPlayList>.from(
          map['images']?.map((x) => ImagesPlayList.fromMap(x))),
      name: map['name'],
      popularity: map['popularity'],
      type: map['type'],
      uri: map['uri'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ArtistDetail.fromJson(String source) =>
      ArtistDetail.fromMap(json.decode(source));
}

class ArtistFollowers {
  String href;
  int total;
  ArtistFollowers({
    required this.href,
    required this.total,
  });

  Map<String, dynamic> toMap() {
    return {
      'href': href,
      'total': total,
    };
  }

  factory ArtistFollowers.fromMap(Map<String, dynamic> map) {
    return ArtistFollowers(
      href: map['href'] ?? '',
      total: map['total'] ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory ArtistFollowers.fromJson(String source) =>
      ArtistFollowers.fromMap(json.decode(source));
}
