import 'dart:convert';

import 'package:prueba_wigilabs/core/model/playlist/playlist_model.dart';

class ResponsePlaylist {
  PlaylistModel? playlists;

  ResponsePlaylist({this.playlists});

  Map<String, dynamic> toMap() {
    return {
      'playlists': playlists?.toMap(),
    };
  }

  factory ResponsePlaylist.fromMap(Map<String, dynamic> map) {
    return ResponsePlaylist(
      playlists: PlaylistModel.fromMap(map['playlists']),
    );
  }

  String toJson() => json.encode(toMap());

  factory ResponsePlaylist.fromJson(String source) =>
      ResponsePlaylist.fromMap(json.decode(source));
}
