import 'dart:convert';

import 'categories_model.dart';

class ResponseCategory {
  Categories? categories;

  ResponseCategory({this.categories});

  Map<String, dynamic> toMap() {
    return {
      'categories': categories?.toMap(),
    };
  }

  factory ResponseCategory.fromMap(Map<String, dynamic> map) {
    return ResponseCategory(
      categories: Categories.fromMap(map['categories']),
    );
  }

  String toJson() => json.encode(toMap());

  factory ResponseCategory.fromJson(String source) =>
      ResponseCategory.fromMap(json.decode(source));
}
