import 'package:flutter/material.dart';
import 'package:prueba_wigilabs/pages/album_page.dart';
import 'package:prueba_wigilabs/pages/artist_page.dart';

import 'pages/home_page.dart';
import 'pages/login_page.dart';
import 'pages/sign_up_page.dart';

class Routes {
  static Route<dynamic>? generateRoutes(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => LoginPage());
      case 'home':
        return MaterialPageRoute(builder: (context) => HomePage());

      case 'signUp':
        return MaterialPageRoute(builder: (context) => SignUpPage());
      default:
    }
  }
}
