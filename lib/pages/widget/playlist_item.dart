import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:prueba_wigilabs/core/blocs/music_cubit/music_cubit.dart';
import 'package:prueba_wigilabs/core/model/playlist/item_playlist_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_wigilabs/pages/music_list_page.dart';

class PlaylistItem extends StatelessWidget {
  final ItemsPlaylist item;
  const PlaylistItem({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MusicCubit, MusicState>(
      builder: (context, state) {
        return InkWell(
          onTap: () {
            context.read<MusicCubit>().getMusicList(item.id);
            pushNewScreen(context,
                screen: MusicListPage(),
                withNavBar: false,
                pageTransitionAnimation: PageTransitionAnimation.slideRight);
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 6.w),
            child: Card(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 6.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 12.h),
                      child: Container(
                        height: 150.h,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: NetworkImage(
                              item.images!.first.url,
                            ),
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                    ),
                    Text(
                      '${item.name}',
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 16.sp),
                    ),
                    Text(
                      '${item.description}',
                      textAlign: TextAlign.left,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
