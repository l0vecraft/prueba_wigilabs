import 'package:flutter/material.dart';
import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomButton extends StatelessWidget {
  final VoidCallback onPress;
  final String title;

  const CustomButton({Key? key, required this.onPress, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: RawMaterialButton(
        onPressed: onPress,
        fillColor: Colors.purple,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 100.w),
          child: Text('$title', style: AppStyles.button1),
        ),
      ),
    );
  }
}
