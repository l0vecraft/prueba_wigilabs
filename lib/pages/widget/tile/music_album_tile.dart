import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:prueba_wigilabs/core/model/artist/artist_album_model.dart';

class MusicAlbumsTile extends StatelessWidget {
  final List<ArtistAlbumItems> albums;
  const MusicAlbumsTile({
    Key? key,
    required this.albums,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate(
        albums
            .sublist(0, 5)
            .map((e) => Padding(
                padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 20.w),
                child: Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: 90.h,
                        width: 90.h,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: NetworkImage(e.images.first.url))),
                      ),
                      SizedBox(width: 12.w),
                      Expanded(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${e.name}',
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: AppStyles.fontBold,
                          ),
                          e.releaseDate.isNotEmpty && e.albumType.isNotEmpty
                              ? Text('${e.releaseDate} - ${e.albumType}')
                              : Container(),
                        ],
                      ))
                    ],
                  ),
                )))
            .toList(),
      ),
    );
  }
}
/**
 * ListTile(
                    leading: Container(
                      height: 120.h,
                      width: 120.w,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(e.images.first.url))),
                    ),
                    title: Text(
                      '${e.name}',
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: AppStyles.fontBold,
                    ),
                    subtitle: e.releaseDate.isNotEmpty && e.albumType.isNotEmpty
                        ? Text('${e.releaseDate} - ${e.albumType}')
                        : null,
                  ),
 */