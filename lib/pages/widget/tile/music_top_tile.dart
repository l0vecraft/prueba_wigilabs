import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:prueba_wigilabs/core/model/artist/artist_album_model.dart';
import 'package:prueba_wigilabs/core/model/top_tracks_model.dart';

class MusicTopTile extends StatelessWidget {
  final List<TopTracksItem> tracks;
  const MusicTopTile({
    Key? key,
    required this.tracks,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate(
        tracks
            .sublist(0, 5)
            .map((e) => Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.h),
                  child: ListTile(
                    leading: Container(
                      height: 60.h,
                      width: 60.w,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(e.album.images.first.url))),
                    ),
                    title: Text(
                      '${tracks.indexOf(e) + 1}. ${e.name}',
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: AppStyles.fontBold,
                    ),
                    subtitle: Text('Popularidad ${e.popularity} '),
                  ),
                ))
            .toList(),
      ),
    );
  }
}
