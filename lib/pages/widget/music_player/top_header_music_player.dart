import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_wigilabs/constants/constants.dart';

class TopHeaderMusicPlayer extends StatelessWidget {
  final String title;
  final String? name;
  const TopHeaderMusicPlayer({
    Key? key,
    required this.title,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(FontAwesomeIcons.chevronDown)),
          Expanded(
            child: Column(
              children: [
                Text('$title',
                    textAlign: TextAlign.center, style: AppStyles.infoStyle1),
                SizedBox(height: 6.h),
                Text(
                  '$name',
                  style: AppStyles.fontBold,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(FontAwesomeIcons.ellipsisV)),
        ],
      ),
    );
  }
}
