import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:prueba_wigilabs/core/blocs/artist_cubit/artist_cubit.dart';
import 'package:prueba_wigilabs/core/model/playlist/music_list_model.dart';
import 'package:prueba_wigilabs/pages/artist_page.dart';

class ModalArtistModal extends StatelessWidget {
  final List<Artists> artists;
  const ModalArtistModal({Key? key, required this.artists}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ArtistCubit, ArtistState>(
      builder: (context, state) {
        return Container(
          height: 280.h,
          child: ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 10.h),
            itemBuilder: (context, i) => SlideInLeft(
              child: ListTile(
                leading: Icon(FontAwesomeIcons.music),
                title: Text('${artists[i].name}', style: AppStyles.fontBold),
                onTap: () {
                  Navigator.pop(context);
                  context.read<ArtistCubit>().getArtistDetail(artists[i].id);
                  pushNewScreen(context, screen: ArtistPage());
                },
              ),
            ),
            itemCount: artists.length,
          ),
        );
      },
    );
  }
}
