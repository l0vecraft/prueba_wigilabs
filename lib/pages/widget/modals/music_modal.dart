import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:prueba_wigilabs/core/blocs/album_cubit/album_cubit.dart';
import 'package:prueba_wigilabs/core/blocs/artist_cubit/artist_cubit.dart';
import 'package:prueba_wigilabs/core/model/playlist/music_list_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_wigilabs/pages/album_page.dart';
import 'package:prueba_wigilabs/pages/artist_page.dart';
import 'package:prueba_wigilabs/pages/widget/modals/music_artists_modal.dart';

class MusicModal extends StatelessWidget {
  final ItemsMusic item;
  final String trackName;
  const MusicModal({Key? key, required this.item, required this.trackName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 650.h,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Image.network(
                    item.track.album!.images.first.url,
                    height: 250.h,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.h),
                  child: Text(
                    '$trackName',
                    textAlign: TextAlign.center,
                    style: AppStyles.musicTitle1,
                  ),
                ),
                Text(
                  item.track.artists!.map((e) => e.name).join(', '),
                  textAlign: TextAlign.center,
                  style: AppStyles.subtitle2,
                )
              ],
            ),
          ),
          BlocBuilder<AlbumCubit, AlbumState>(
            builder: (context, state) {
              return ListTile(
                  leading: Icon(FontAwesomeIcons.compactDisc),
                  title: Text('Ver álbum',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  onTap: () {
                    Navigator.pop(context);
                    context
                        .read<AlbumCubit>()
                        .getAlbumTracks(item.track.album!.id);
                    pushNewScreen(context, screen: AlbumPage());
                  });
            },
          ),
          BlocBuilder<ArtistCubit, ArtistState>(
            builder: (context, state) {
              return ListTile(
                leading: Icon(FontAwesomeIcons.music),
                title: Text('Ver artistas',
                    style: TextStyle(fontWeight: FontWeight.bold)),
                onTap: () {
                  if (item.track.artists!.length > 1) {
                    Navigator.pop(context);
                    showModalBottomSheet(
                        context: context,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        builder: (context) =>
                            ModalArtistModal(artists: item.track.artists!));
                  } else {
                    Navigator.pop(context);
                    context
                        .read<ArtistCubit>()
                        .getArtistDetail(item.track.artists!.first.id);
                    pushNewScreen(context, screen: ArtistPage());
                  }
                },
              );
            },
          ),
          SizedBox(height: 10.h)
        ],
      ),
    );
  }
}
