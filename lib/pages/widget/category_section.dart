import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:prueba_wigilabs/core/blocs/playlist_cubit/playlist_cubit.dart';
import 'package:prueba_wigilabs/core/model/item_category_model.dart';
import 'package:prueba_wigilabs/pages/widget/custom_progress.dart';
import 'package:prueba_wigilabs/pages/widget/playlist_item.dart';

class CategorySection extends StatefulWidget {
  final ItemCategory category;

  const CategorySection({Key? key, required this.category}) : super(key: key);

  @override
  _CategorySectionState createState() => _CategorySectionState();
}

class _CategorySectionState extends State<CategorySection> {
  @override
  void initState() {
    super.initState();
    Provider.of<PlaylistCubit>(context, listen: false)
        .getPlayList(widget.category.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: BlocConsumer<PlaylistCubit, PlaylistState>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state is PlaylistLoaded) {
              return CustomScrollView(
                slivers: [
                  SliverPersistentHeader(
                    delegate: CustomHeader(
                        url: widget.category.icons!.first.url,
                        categoryName: widget.category.name!),
                    pinned: true,
                  ),
                  // SliverAppBar(
                  //     floating: true,
                  //     expandedHeight: 250.h,
                  //     flexibleSpace: Container(
                  //       height: 280.h,
                  //       alignment: Alignment.bottomCenter,
                  //       decoration: BoxDecoration(
                  //           image: DecorationImage(
                  //               image: NetworkImage(
                  //                   widget.category.icons!.first.url),
                  //               fit: BoxFit.cover)),
                  //       child: Padding(
                  //         padding: EdgeInsets.only(bottom: 10.h),
                  //         child: Text(
                  //           '${widget.category.name}',
                  //           style: AppStyles.categoryTitle2,
                  //         ),
                  //       ),
                  //     )),
                  SliverToBoxAdapter(child: SizedBox(height: 30.h)),
                  SliverGrid.count(
                    crossAxisCount: 2,
                    crossAxisSpacing: 2.w,
                    mainAxisSpacing: 6.w,
                    childAspectRatio: .7,
                    children: state.playlist.playlists!.items
                        .map((e) => PlaylistItem(item: e))
                        .toList(),
                  ),
                  SliverToBoxAdapter(child: SizedBox(height: 20.h)),
                ],
              );
            } else {
              return Container(height: 400.h, child: CustomProgressIndicator());
            }
          },
        ),
      ),
    );
  }
}

class CustomHeader extends SliverPersistentHeaderDelegate {
  final String url;
  final String categoryName;
  CustomHeader({
    required this.url,
    required this.categoryName,
  });

  static const _maxExtend = 280.0;
  static const _minExtend = 100.0;

  var _maxTitlePositioned = 100.w;
  var _minTitlePositioned = 50.w;
  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    var _percent = shrinkOffset / _maxExtend;
    print(_percent);
    return Container(
      color: Colors.black,
      child: Stack(
        alignment: Alignment.center,
        children: [
          AnimatedOpacity(
            duration: Duration(milliseconds: 350),
            opacity: 1 - _percent,
            child: Container(
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(url), fit: BoxFit.fill, scale: .5),
                )),
          ),
          Positioned(
            left: _percent >= 0.15
                ? (_maxTitlePositioned * (1 - _percent))
                    .clamp(_minTitlePositioned, _maxTitlePositioned)
                : null,
            bottom: 35.h,
            child: Text('$categoryName',
                style: TextStyle(
                    fontSize: (30.sp * (1 - _percent)).clamp(20.sp, 30.sp),
                    fontWeight: FontWeight.bold,
                    color: Colors.white)),
          ),
          Positioned(
            left: 0,
            top: 28.h,
            child: IconButton(
                onPressed: () => Navigator.pop(context),
                icon: Icon(Icons.arrow_back)),
          )
        ],
      ),
    );
  }

  @override
  double get maxExtent => _maxExtend;

  @override
  double get minExtent => _minExtend;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
