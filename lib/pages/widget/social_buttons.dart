import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_wigilabs/core/blocs/auth/auth_cubit.dart';

class SocialButtons extends StatelessWidget {
  const SocialButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 18.h),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            BlocBuilder<AuthCubit, AuthState>(
              builder: (context, state) {
                return GestureDetector(
                  onTap: () => context.read<AuthCubit>().singInGoogle(),
                  child: Material(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    child: Padding(
                      padding: EdgeInsets.all(10.h),
                      child: Icon(
                        FontAwesomeIcons.google,
                        color: Colors.red,
                        size: 35.h,
                      ),
                    ),
                    elevation: 5,
                  ),
                );
              },
            ),
            SizedBox(width: 20.w),
            BlocBuilder<AuthCubit, AuthState>(
              builder: (context, state) {
                return GestureDetector(
                  onTap: () => context.read<AuthCubit>().singInFacebook(),
                  child: Material(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    child: Padding(
                      padding: EdgeInsets.all(10.h),
                      child: Icon(
                        FontAwesomeIcons.facebookF,
                        color: Colors.blue,
                        size: 35.h,
                      ),
                    ),
                    elevation: 5,
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
