import 'package:flutter/material.dart';
import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomFormField extends StatelessWidget {
  final TextEditingController controller;
  final String text;
  final Function? validator;
  final Function? onSaved;
  final Function? onSubmited;
  final bool obscure;
  const CustomFormField(
      {Key? key,
      required this.controller,
      required this.text,
      this.validator,
      this.obscure = false,
      this.onSaved,
      this.onSubmited})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 18.w),
      child: TextFormField(
        controller: controller,
        obscureText: obscure,
        keyboardType: obscure
            ? TextInputType.visiblePassword
            : TextInputType.emailAddress,
        decoration: InputDecoration(
            labelText: text,
            border: AppStyles.circularBorder,
            enabledBorder: AppStyles.circularBorder,
            focusedBorder: AppStyles.circularBorder),
        validator: (value) => validator!(value),
        onFieldSubmitted: (value) => onSaved!(value),
        onSaved: (value) => onSubmited!(value),
      ),
    );
  }
}
