import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:prueba_wigilabs/core/blocs/music_cubit/music_cubit.dart';
import 'package:intl/intl.dart';
import 'package:prueba_wigilabs/pages/music_player.dart';
import 'package:prueba_wigilabs/pages/widget/custom_progress.dart';
import 'package:prueba_wigilabs/pages/widget/modals/music_modal.dart';

import 'widget/appbar/custom_appbar.dart';

class MusicListPage extends StatefulWidget {
  const MusicListPage({Key? key}) : super(key: key);

  @override
  _MusicListPageState createState() => _MusicListPageState();
}

class _MusicListPageState extends State<MusicListPage> {
  var _format = NumberFormat('###,###,###,###', 'en_US');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<MusicCubit, MusicState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is MusicLoaded) {
            return Container(
              child: CustomScrollView(
                slivers: [
                  CustomAppBar(
                    imageUrl: state.listMusic.images.first.url,
                  ),
                  SliverToBoxAdapter(
                    child: Container(
                      padding: EdgeInsets.only(left: 12.w, bottom: 6.h),
                      color: Colors.black,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            '${state.listMusic.description}',
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.left,
                            style: AppStyles.subtitle2,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 2.w, vertical: 12.h),
                            child: Text(
                              '${_format.format(state.listMusic.followers.total)} Followers.',
                              textAlign: TextAlign.left,
                              style: AppStyles.subtitle2,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SliverReorderableList(
                      itemBuilder: (context, i) => ListTile(
                            onTap: () {
                              context.read<MusicCubit>().getTrack(
                                  state.listMusic.tracks.items[i].track);
                              pushNewScreen(context,
                                  screen: MusicPlayer(),
                                  pageTransitionAnimation:
                                      PageTransitionAnimation.slideUp);
                            },
                            leading: Container(
                              width: 60.w,
                              height: 60.h,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: NetworkImage(state
                                          .listMusic
                                          .tracks
                                          .items[i]
                                          .track
                                          .album!
                                          .images
                                          .first
                                          .url))),
                            ),
                            key: Key(i.toString()),
                            title: Text(
                                '${state.listMusic.tracks.items[i].track.name}'),
                            subtitle: Text(
                              state.listMusic.tracks.items[i].track.artists!
                                  .map((e) => e.name)
                                  .join(', '),
                              overflow: TextOverflow.ellipsis,
                            ),
                            trailing: IconButton(
                                onPressed: () {
                                  showModalBottomSheet(
                                      context: context,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30)),
                                      builder: (context) => MusicModal(
                                          item: state.listMusic.tracks.items[i],
                                          trackName: state.listMusic.tracks
                                              .items[i].track.name!));
                                },
                                icon: Icon(FontAwesomeIcons.ellipsisV,
                                    size: 18.sp)),
                          ),
                      itemCount: state.listMusic.tracks.items.length,
                      onReorder: (a, b) {})
                ],
              ),
            );
          } else {
            return CustomProgressIndicator();
          }
        },
      ),
    );
  }
}
