import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:marquee/marquee.dart';
import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:prueba_wigilabs/core/blocs/music_cubit/music_cubit.dart';
import 'package:prueba_wigilabs/pages/widget/custom_progress.dart';
import 'package:prueba_wigilabs/pages/widget/music_player/top_header_music_player.dart';

class MusicPlayer extends StatefulWidget {
  const MusicPlayer({Key? key}) : super(key: key);

  @override
  _MusicPlayerState createState() => _MusicPlayerState();
}

class _MusicPlayerState extends State<MusicPlayer>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  var _isPlay = false;
  var _currentValue = 0.0;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 400));
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: AppStyles.basicDecoration,
        child: BlocConsumer<MusicCubit, MusicState>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state is MusicLoaded) {
              return ListView(
                children: [
                  TopHeaderMusicPlayer(
                    title: 'Repoduciendo',
                    name: state.trackSelected!.name,
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 40.h, horizontal: 30.w),
                    child: Image.network(
                      state.trackSelected!.album!.images.first.url,
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 12.h, horizontal: 30.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        state.trackSelected!.name!.length < 20
                            ? Text(
                                '${state.trackSelected!.name}',
                                style: AppStyles.albumTitle1,
                              )
                            : Container(
                                height: 40.h,
                                child: Marquee(
                                  text: '${state.trackSelected!.name}',
                                  style: AppStyles.albumTitle1,
                                  numberOfRounds: 2,
                                ),
                              ),
                        SizedBox(height: 5.h),
                        Text(
                            '${state.trackSelected!.artists!.map((e) => e.name).join(", ")}',
                            style: AppStyles.subtitle2)
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 12.h, horizontal: 30.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('00:00'),
                        Text('${state.trackSelected!.durationMs!}')
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.w),
                    child: SliderTheme(
                      data: SliderThemeData(
                          trackHeight: 3,
                          thumbShape:
                              RoundSliderThumbShape(enabledThumbRadius: 5.sp)),
                      child: Slider(
                        value: _currentValue,
                        activeColor: Colors.black45,
                        inactiveColor: Colors.black12,
                        max: 100,
                        min: 0,
                        onChanged: (value) {
                          setState(() {
                            _currentValue = value;
                          });
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 25.h, horizontal: 30.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                            onPressed: () {},
                            icon: Icon(FontAwesomeIcons.stepBackward,
                                size: 30.sp)),
                        InkWell(
                          onTap: () {
                            setState(() {
                              _isPlay = !_isPlay;
                            });
                            _isPlay
                                ? _controller.forward()
                                : _controller.reverse();
                          },
                          child: AnimatedIcon(
                              icon: AnimatedIcons.play_pause,
                              size: 50.sp,
                              progress: _controller),
                        ),
                        IconButton(
                            onPressed: () {},
                            icon: Icon(FontAwesomeIcons.stepForward,
                                size: 30.sp)),
                      ],
                    ),
                  )
                ],
              );
            } else {
              return CustomProgressIndicator();
            }
          },
        ),
      ),
    );
  }
}
