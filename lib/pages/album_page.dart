import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:prueba_wigilabs/core/blocs/album_cubit/album_cubit.dart';
import 'package:prueba_wigilabs/pages/widget/appbar/custom_appbar.dart';
import 'package:prueba_wigilabs/pages/widget/custom_progress.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

class AlbumPage extends StatelessWidget {
  const AlbumPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocConsumer<AlbumCubit, AlbumState>(
      listener: (context, state) {},
      builder: (context, state) {
        if (state is AlbumLoaded) {
          return Container(
            child: CustomScrollView(
              slivers: [
                CustomAppBar(imageUrl: state.albumModel.images.first.url),
                SliverToBoxAdapter(
                  child: Container(
                    padding: EdgeInsets.only(left: 12.w, bottom: 6.h),
                    color: Colors.black,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          '${state.albumModel.name}',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.left,
                          style: AppStyles.albumTitle1,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 6.h),
                          child: Text(
                            '${state.albumModel.artists.first.name}',
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.left,
                            style: AppStyles.albumSubTitle1,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 2.w, vertical: 12.h),
                          child: Text(
                            state.albumModel.releaseDatePrecision.isEmpty
                                ? 'Álbum'
                                : 'Álbum - ${state.albumModel.releaseDate}',
                            textAlign: TextAlign.left,
                            style: AppStyles.subtitle2,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SliverReorderableList(
                    itemBuilder: (context, i) => ListTile(
                          leading: Container(
                            width: 60.w,
                            height: 60.h,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: NetworkImage(
                                        state.albumModel.images.first.url))),
                          ),
                          key: Key(i.toString()),
                          title:
                              Text('${state.albumModel.tracks.items[i].name}'),
                          subtitle: Text(
                            state.albumModel.tracks.items[i].artists
                                .map((e) => e.name)
                                .join(', '),
                            overflow: TextOverflow.ellipsis,
                          ),
                          trailing: IconButton(
                              onPressed: () {},
                              icon: Icon(FontAwesomeIcons.ellipsisV,
                                  size: 18.sp)),
                        ),
                    itemCount: state.albumModel.tracks.items.length,
                    onReorder: (a, b) {})
              ],
            ),
          );
        } else {
          return CustomProgressIndicator();
        }
      },
    ));
  }
}
