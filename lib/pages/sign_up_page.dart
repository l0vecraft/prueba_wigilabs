import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:prueba_wigilabs/core/blocs/auth/auth_cubit.dart';
import 'package:prueba_wigilabs/core/model/user_auth_model.dart';
import 'package:prueba_wigilabs/core/validators/validators.dart';
import 'package:prueba_wigilabs/pages/widget/custom_form_field.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_wigilabs/pages/widget/custom_progress.dart';
import 'package:prueba_wigilabs/pages/widget/social_buttons.dart';

import 'widget/button/custom_button.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  var _email = TextEditingController();

  var _password = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        alignment: Alignment.center,
        child: GestureDetector(
          onTap: () {
            FocusNode _focus = FocusScope.of(context);
            if (!_focus.hasPrimaryFocus) _focus.unfocus();
          },
          child: BlocConsumer<AuthCubit, AuthState>(
            listener: (context, state) {
              if (state is AuthLoading) {
                showDialog(
                    context: context,
                    builder: (context) => Dialog(
                          child: Container(
                              height: 100.h,
                              width: 100.w,
                              child: CustomProgressIndicator()),
                        ));
              }
              if (state is AuthSignIn) {
                Navigator.pop(context);
                Navigator.pushNamedAndRemoveUntil(
                    context, 'home', (route) => false);
              }
              if (state is AuthError) {
                Navigator.pop(context);
                showDialog(
                    context: context,
                    builder: (context) => Dialog(
                          child: Container(
                            height: 100.h,
                            width: 100.w,
                            child: Center(
                              child: Text(state.message),
                            ),
                          ),
                        ));
              }
            },
            builder: (context, state) {
              return Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      FlutterLogo(size: 110.h),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.h),
                        child: Text(
                          'Register Page',
                          style: AppStyles.title1,
                        ),
                      ),
                      CustomFormField(
                        controller: _email,
                        text: 'email',
                        validator: (value) {
                          return Validators.emailField(value) ??
                              Validators.requiredField(value);
                        },
                        onSaved: (value) {
                          setState(() {
                            _email.text = value;
                          });
                        },
                        onSubmited: (value) {
                          setState(() {
                            _email.text = value;
                          });
                        },
                      ),
                      CustomFormField(
                        controller: _password,
                        text: 'password',
                        obscure: true,
                        validator: (value) => Validators.requiredField(value),
                        onSaved: (value) {
                          setState(() {
                            _password.text = value;
                          });
                        },
                        onSubmited: (value) {
                          setState(() {
                            _password.text = value;
                          });
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: 20.h,
                        ),
                        child: RichText(
                            text: TextSpan(children: [
                          TextSpan(text: '¿Ya tienes una cuenta?'),
                          TextSpan(
                              text: ' Ingresa aqui',
                              style: TextStyle(color: Colors.purple),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  Navigator.pushReplacementNamed(context, '/');
                                })
                        ])),
                      ),
                      SizedBox(height: 30.h),
                      BlocBuilder<AuthCubit, AuthState>(
                        builder: (context, state) {
                          return CustomButton(
                            onPress: () {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();
                                var user = UserAuth(
                                    email: _email.text,
                                    password: _password.text);
                                context
                                    .read<AuthCubit>()
                                    .singUpEmailAndPassword(user);
                              }
                            },
                            title: 'LOGIN',
                          );
                        },
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
