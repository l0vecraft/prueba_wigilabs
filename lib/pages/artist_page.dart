import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:prueba_wigilabs/core/blocs/artist_cubit/artist_cubit.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_wigilabs/core/model/artist/artist_album_model.dart';
import 'package:prueba_wigilabs/pages/widget/custom_progress.dart';
import 'package:intl/intl.dart';
import 'package:prueba_wigilabs/pages/widget/tile/music_album_tile.dart';
import 'package:prueba_wigilabs/pages/widget/tile/music_top_tile.dart';

class ArtistPage extends StatelessWidget {
  const ArtistPage({Key? key}) : super(key: key);

  static var _format = NumberFormat('###,###,###', 'en_US');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocConsumer<ArtistCubit, ArtistState>(
      listener: (context, state) {},
      builder: (context, state) {
        if (state is ArtistLoaded) {
          return Container(
            child: CustomScrollView(
              slivers: [
                SliverAppBar(
                  expandedHeight: 300.h,
                  flexibleSpace: Stack(
                    alignment: Alignment.bottomLeft,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image:
                                    NetworkImage(state.artist.images.first.url),
                                fit: BoxFit.cover)),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 10.w, bottom: 5.h),
                        width: double.infinity,
                        color: Colors.black.withOpacity(.2),
                        child: Text(
                          state.artist.name,
                          style: AppStyles.categoryTitle2,
                        ),
                      )
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    padding: EdgeInsets.only(left: 12.w, bottom: 6.h),
                    color: Colors.black,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 2.w, vertical: 12.h),
                          child: Text(
                            '${_format.format(state.artist.followers.total)} Followers.',
                            textAlign: TextAlign.left,
                            style: AppStyles.subtitle2,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                    child: Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 10.h, horizontal: 18.w),
                  child: Text(
                    'Populares',
                    style: AppStyles.subtitle3,
                  ),
                )),
                MusicTopTile(tracks: state.topTracks.tracks),
                SliverToBoxAdapter(
                    child: Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 10.h, horizontal: 18.w),
                  child: Text(
                    'Lanzamientos populares',
                    style: AppStyles.subtitle3,
                  ),
                )),
                MusicAlbumsTile(
                  albums: state.albums.items,
                ),
              ],
            ),
          );
        } else {
          return CustomProgressIndicator();
        }
      },
    ));
  }
}
