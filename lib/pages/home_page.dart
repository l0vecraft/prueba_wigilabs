import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:prueba_wigilabs/constants/constants.dart';
import 'package:prueba_wigilabs/core/blocs/auth/auth_cubit.dart';
import 'package:prueba_wigilabs/core/blocs/spotify_cubit/spotify_cubit.dart';
import 'package:prueba_wigilabs/pages/widget/category_card.dart';
import 'package:prueba_wigilabs/pages/widget/category_section.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'widget/custom_progress.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      await _checkAuthSpotify();
    });
  }

  Future<void> _checkAuthSpotify() async {
    final _prefs = await SharedPreferences.getInstance();
    var _isAuth = _prefs.getString('spoty_token') == null;
    if (_isAuth) {
      await context.read<SpotifyCubit>().getAuthInSpoty();
    } else {
      context.read<SpotifyCubit>().getCategories();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          BlocConsumer<AuthCubit, AuthState>(
              listener: (context, state) {
                if (state is AuthLoading) {
                  showDialog(
                      context: context,
                      builder: (context) => Dialog(
                            child: Container(
                                height: 100.h,
                                width: 100.w,
                                child: CustomProgressIndicator()),
                          ));
                }
                if (state is AuthSignOut) {
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/', (route) => false);
                }
              },
              builder: (context, state) => IconButton(
                  onPressed: () => context.read<AuthCubit>().signOut(),
                  icon: Icon(Icons.exit_to_app)))
        ],
      ),
      body: BlocConsumer<SpotifyCubit, SpotifyState>(
        listener: (context, state) {
          if (state is SpotifyLoading) {
            showDialog(
                context: context,
                builder: (context) => Dialog(
                      child: Container(
                          height: 100.h,
                          width: 100.w,
                          child: CustomProgressIndicator()),
                    ));
          }
          if (state is SpotifyAuth) {
            Navigator.pop(context);
          }
        },
        builder: (context, state) {
          if (state is SpotifyLoading) {
            return CustomProgressIndicator();
          } else if (state is SpotifyLoaded) {
            return SafeArea(
              child: Container(
                  child: CustomScrollView(
                slivers: [
                  SliverList(
                      delegate: SliverChildListDelegate([
                    Padding(
                      padding:
                          EdgeInsets.only(top: 15.h, left: 15.w, bottom: 8.h),
                      child: Text(
                        'Hola de nuevo',
                        textAlign: TextAlign.left,
                        style: AppStyles.title2,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15.w, bottom: 8.h),
                      child: Text('¿Que escucharemos hoy?',
                          textAlign: TextAlign.left,
                          style: AppStyles.subtitle1),
                    ),
                  ])),
                  SliverToBoxAdapter(
                    child: Container(
                      height: 200.h,
                      child: ListView.builder(
                        itemCount: state.categories.categories!.items!.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, i) => OpenContainer(
                          openBuilder: (context, action) => CategorySection(
                              category: state.categories.categories!.items![i]),
                          closedBuilder: (context, action) => CategoryCard(
                              category: state.categories.categories!.items![i]),
                          closedElevation: 0,
                          closedColor: Colors.black12,
                        ),
                      ),
                    ),
                  )
                ],
              )),
            );
          } else if (state is SpotifyError) {
            return Container(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Es posible que el Token haya vencido.',
                      textAlign: TextAlign.center, style: AppStyles.subtitle3),
                  SizedBox(height: 20.h),
                  RawMaterialButton(
                      fillColor: Colors.purple,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)),
                      onPressed: () {
                        context.read<SpotifyCubit>().getAuthInSpoty();
                        Provider.of<SpotifyCubit>(context, listen: false)
                            .getCategories();
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 10.h, horizontal: 100.w),
                        child: Text(
                          'Re-Autenticar',
                          style: TextStyle(fontSize: 18.sp),
                        ),
                      )),
                ],
              ),
            );
          } else {
            return CustomProgressIndicator();
          }
        },
      ),
    );
  }
}
