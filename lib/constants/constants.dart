import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppStyles {
  static TextStyle fontBold = TextStyle(fontWeight: FontWeight.bold);
  static TextStyle title1 =
      TextStyle(fontSize: 23.sp, fontWeight: FontWeight.bold);
  static TextStyle title2 =
      TextStyle(fontSize: 30.sp, fontWeight: FontWeight.bold);

  static TextStyle subtitle1 = TextStyle(fontSize: 18.sp, color: Colors.grey);
  static TextStyle subtitle2 = TextStyle(fontSize: 14.sp, color: Colors.grey);
  static TextStyle subtitle3 =
      TextStyle(fontSize: 18.sp, fontWeight: FontWeight.bold);
  static TextStyle button1 = TextStyle(color: Colors.white, fontSize: 20.sp);
  static TextStyle categoryTitle1 = TextStyle(
      color: Colors.white, fontSize: 20.sp, fontWeight: FontWeight.bold);
  static TextStyle categoryTitle2 = TextStyle(
      fontSize: 30.sp, fontWeight: FontWeight.bold, color: Colors.white);
  static TextStyle musicTitle1 =
      TextStyle(fontSize: 20.sp, fontWeight: FontWeight.bold);
  static TextStyle albumTitle1 = TextStyle(
      fontSize: 22.sp, color: Colors.white, fontWeight: FontWeight.bold);
  static TextStyle albumSubTitle1 = TextStyle(
      fontSize: 16.sp, color: Colors.white, fontWeight: FontWeight.bold);
  static TextStyle infoStyle1 =
      TextStyle(fontWeight: FontWeight.w100, fontSize: 18.sp);

  /*FIELDS*/
  static OutlineInputBorder circularBorder =
      OutlineInputBorder(borderRadius: BorderRadius.circular(30));
  /* Decorations */
  static BoxDecoration basicDecoration = BoxDecoration(
      gradient: LinearGradient(
          colors: [Colors.purple, Colors.black],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight));
}
