import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:prueba_wigilabs/core/model/failure.dart';
import 'package:prueba_wigilabs/core/model/user_auth_model.dart';
import 'package:prueba_wigilabs/core/model/user_response.dart';
import 'package:prueba_wigilabs/data/repository/auth_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

class AuthApi implements AuthRepository {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  @override
  Future<UserResponse> signInEmailPassword(UserAuth user) async {
    late UserResponse response;
    var prefs = await _prefs;
    try {
      var _user = await _auth.signInWithEmailAndPassword(
          email: user.email, password: user.password);
      if (_user != null) {
        var _token = await _user.user!.getIdToken();
        await prefs.setString('token', _token);
        await prefs.setString('email', user.email);
        response = UserResponse(email: user.email, token: _token);
      }
    } on Exception {
      throw Failure(message: "Error, no se pudo iniciar sesion");
    }
    return response;
  }

  @override
  Future signOut() async {
    final facebook = FacebookAuth.instance;
    var token = await facebook.accessToken;
    try {
      await _auth.signOut();
      // if (token != null) facebook.logOut();
    } on Exception {
      throw Failure(message: 'Error al salir de la sesion');
    }
  }

  @override
  Future<UserResponse> singInFacebook() async {
    UserResponse response = UserResponse();
    final _prefs = await SharedPreferences.getInstance();
    try {
      final facebook = await FacebookAuth.instance.login();
      print(facebook.message);
      if (facebook.status == LoginStatus.success) {
        final accessToken = facebook.accessToken;
        final OAuthCredential credential =
            FacebookAuthProvider.credential(accessToken!.token);
        _auth.signInWithCredential(credential);
        _prefs.setString('token', credential.token.toString());
        response = UserResponse(
            token: credential.token.toString(), email: 'example.com');
      }
    } on Exception {
      throw Failure(message: 'Error al iniciar sesion');
    }
    print('EL TOKEN${response.token}');
    return response;
  }

  @override
  Future<UserResponse> singInGoogle() async {
    UserResponse response = UserResponse();
    final _prefs = await SharedPreferences.getInstance();
    try {
      final google = await _googleSignIn.signIn();
      final GoogleSignInAuthentication authGoogle =
          await google!.authentication;
      final AuthCredential credentials = GoogleAuthProvider.credential(
          accessToken: authGoogle.accessToken, idToken: authGoogle.idToken);
      String? token = authGoogle.accessToken;
      _auth.signInWithCredential(credentials);
      response = UserResponse(email: 'prueba@prueba.com', token: token);
      _prefs.setString('token', token!);
    } on Exception {
      throw Failure(
          message: 'Error no se pudo iniciar sesion con la cuenta de google');
    }
    return response;
  }

  @override
  Future<UserResponse> singUpEmailPassword(UserAuth user) async {
    UserResponse response = UserResponse();
    var prefs = await _prefs;
    try {
      var _user = await _auth.createUserWithEmailAndPassword(
          email: user.email, password: user.password);
      if (_user != null) {
        var _token = await _user.user!.getIdToken();
        prefs.setString('token', _token);
        prefs.setString('email', user.email);

        response = UserResponse(email: user.email, token: _token);
      }
    } on Exception {
      throw Failure(message: "Error, no se pudo iniciar sesion");
    }
    return response;
  }

  @override
  Future<bool> isSignIn() async {
    bool? isSignIn;
    try {
      isSignIn = await _googleSignIn.isSignedIn();
    } on Exception {
      throw Failure(message: 'ha ocurrido un error al comprobar el estado');
    }
    return isSignIn;
  }

  @override
  Future<void> singOutGoogle() async {
    try {
      await _auth.signOut();
      await _googleSignIn.signOut();
    } on Exception {
      throw Failure(message: 'Error al salir de la sesion');
    }
  }
}
