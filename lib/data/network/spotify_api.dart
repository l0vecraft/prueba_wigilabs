import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_web_auth/flutter_web_auth.dart';
import 'package:prueba_wigilabs/core/model/album_model.dart';
import 'package:prueba_wigilabs/core/model/artist/artist_album_model.dart';
import 'package:prueba_wigilabs/core/model/artist/artist_detail_model.dart';
import 'package:prueba_wigilabs/core/model/failure.dart';
import 'package:prueba_wigilabs/core/model/playlist/music_list_model.dart';
import 'package:prueba_wigilabs/core/model/response_category_model.dart';
import 'package:prueba_wigilabs/core/model/response_playlist_model.dart';
import 'package:prueba_wigilabs/core/model/top_tracks_model.dart';
import 'package:prueba_wigilabs/data/repository/spotify_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SpotifyApi implements SpotifyRepository {
  final _dio = Dio();
  String _urlBaseAuth = 'https://accounts.spotify.com/authorize';
  String _urlBaseToken = 'https://accounts.spotify.com/api/token';
  String _urlBaseCategories = 'https://api.spotify.com/v1/browse/categories';
  String _urlBasePlaylists = 'https://api.spotify.com/v1/playlists';
  String _urlBaseAlbums = 'https://api.spotify.com/v1/albums';
  String _urlBaseArtists = 'https://api.spotify.com/v1/artists';
  String _cid = 'def7e328e5f04762bec4bc0097949cb8';
  String _secret = '4effcfb64e574a998e3a6ad2e867e04c';
  String _responseType = 'code';
  String _redirect = 'wigilabs://myapp';
  String _state = '34fFs29kd09';

  @override
  Future changeCountry(String token) {
    // TODO: implement changeCountry
    throw UnimplementedError();
  }

  @override
  Future<String> getAccessToken() async {
    final _prefs = await SharedPreferences.getInstance();
    Codec _encoder = utf8.fuse(base64);
    var auth = _encoder.encode("$_cid:$_secret");
    var result = '';
    var token = '';
    var url =
        "$_urlBaseAuth?client_id=$_cid&response_type=$_responseType&redirect_uri=$_redirect&scope=user-read-private%20user-read-email&state=$_state";
    try {
      final response = await FlutterWebAuth.authenticate(
          url: url, callbackUrlScheme: "wigilabs");
      final error = Uri.parse(response).queryParameters['error'];
      if (error == null) {
        result = Uri.parse(response).queryParameters['code']!;
      } else {
        result = error;
      }
      var res = await _dio.post(
        '$_urlBaseToken',
        data: {
          'grant_type': 'authorization_code',
          'code': '$result',
          'redirect_uri': '$_redirect'
        },
        options: Options(
            contentType: 'application/x-www-form-urlencoded',
            headers: {'Authorization': 'Basic $auth'}),
      );
      token = res.data['access_token'];
      _prefs.setString('spoty_token', token);
    } on Exception {
      throw Failure(message: 'Error no se pudo autenticar');
    }
    return token;
  }

  @override
  Future listPlayList(String token, String idCategory,
      [String country = 'CO']) async {
    Map<String, dynamic> _headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      final response = await _dio.get(
          '$_urlBaseCategories/$idCategory/playlists?country=$country',
          options: Options(headers: _headers));

      var result = ResponsePlaylist.fromMap(response.data);
      return result;
    } on Exception {
      throw Failure(message: 'Error al obtener las playlists');
    }
  }

  @override
  Future listCategories(String? token, [String country = 'CO']) async {
    Map<String, dynamic> _headers = {
      "Content-Type": "application/json",
      // "Authorization": "Bearer $token"
      "Authorization": "Bearer $token"
    };
    try {
      final response = await _dio.get('$_urlBaseCategories?country=$country',
          options: Options(headers: _headers));

      var result = ResponseCategory.fromMap(response.data);
      return result;
    } on Exception {
      throw Failure(message: 'Error al obtener las categorias');
    }
  }

  @override
  Future getAlbumOfArtist(String token, String? idAlbum) async {
    Map<String, dynamic> _headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      final response = await _dio.get('$_urlBaseAlbums/$idAlbum',
          options: Options(headers: _headers));

      var result = AlbumModel.fromMap(response.data);
      return result;
    } on Exception {
      throw Failure(message: 'Error al obtener el album');
    }
  }

  @override
  Future getArtistDetail(String token, String? idArtist) async {
    Map<String, dynamic> _headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      final response = await _dio.get('$_urlBaseArtists/$idArtist',
          options: Options(headers: _headers));

      var result = ArtistDetail.fromMap(response.data);
      return result;
    } on Exception {
      throw Failure(message: 'Error al obtener el artista');
    }
  }

  Future getArtistAlbums(String token, String? idArtist) async {
    Map<String, dynamic> _headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      final response = await _dio.get('$_urlBaseArtists/$idArtist/albums',
          options: Options(headers: _headers));

      var result = ArtistAlbum.fromMap(response.data);
      return result;
    } on Exception {
      throw Failure(message: 'Error al obtener las categorias');
    }
  }

  @override
  Future getSongsOfPlayList(String token, String? idPlaylist) async {
    Map<String, dynamic> _headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      final response = await _dio.get('$_urlBasePlaylists/$idPlaylist',
          options: Options(headers: _headers));

      var result = MusicListModel.fromMap(response.data);
      return result;
    } on Exception {
      throw Failure(message: 'Error al obtener la lista de canciones');
    }
  }

  @override
  Future getTopSongsArtist(String token, String? idArtist,
      [String country = 'CO']) async {
    Map<String, dynamic> _headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      final response = await _dio.get(
          '$_urlBaseArtists/$idArtist/top-tracks?market=$country',
          options: Options(headers: _headers));

      var result = TopTracks.fromMap(response.data);
      return result;
    } on Exception {
      throw Failure(message: 'Error al obtener las mejores canciones');
    }
  }
}
