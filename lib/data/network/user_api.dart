import 'package:dio/dio.dart';
import 'package:prueba_wigilabs/data/repository/user_repository.dart';

class UserApi implements UserRepository {
  static String _urlBase =
      'https://apiselfservice.co/api/index.php/v1/soap/LoginUsuario.json';
  final _dio = Dio();
  @override
  Future getData() async {
    try {
      var response = _dio.post(_urlBase,
          data: {
            "data": {"nombreUsuario": "a@a.aa", "clave": "Garay1362"}
          },
          options: Options(headers: {'X-MC-SO': 'WigilabsTest'}));
      response.then((value) => print(value.data));
    } catch (e) {
      print(e);
    }
  }
}
