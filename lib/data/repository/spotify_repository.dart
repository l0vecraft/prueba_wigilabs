abstract class SpotifyRepository {
  Future<String> getAccessToken();
  Future listCategories(String? token, [String country = 'CO']);
  Future listPlayList(String token, String idCategory, [String country = 'CO']);
  Future getSongsOfPlayList(String token, String? idPlaylist);
  Future getArtistDetail(String token, String? idArtist);
  Future getArtistAlbums(String token, String? idArtist);
  Future getAlbumOfArtist(String token, String? idArtist);
  Future getTopSongsArtist(String token, String? idArtist,
      [String country = 'CO']);
  Future changeCountry(String token);
}
