import 'package:prueba_wigilabs/core/model/user_auth_model.dart';
import 'package:prueba_wigilabs/core/model/user_response.dart';

abstract class AuthRepository {
  Future<UserResponse> signInEmailPassword(UserAuth user);
  Future<UserResponse> singUpEmailPassword(UserAuth user);
  Future<UserResponse> singInGoogle();
  Future<UserResponse> singInFacebook();
  Future<bool> isSignIn();
  Future<void> singOutGoogle();
  Future signOut();
}
