# prueba wigilabs

En este repositorio se encuentra la prueba técnica presentada a wigilabs, la cual consistía en consumir muchos de los servicios de la API de Spotify. Como manejador de estados se decidió utilizar Cubits de la librería de flutter_blocs la cual es una variante mucho más sencilla de los tradicionales Blocs y es mucho más facial organizar para realizar pruebas y manejar excepciones.

La arquitectura seguida para el desarrollo del proyecto fue clean architecture con unas cuantas modificaciones siguiendo DDD, debido a que podemos encontrar las siguientes carpetas.

* core: para toda la logica
* data: para los llamados a apis y otras implementaciones
* constants: archivos estaticos y que pueden ser re utilizados como estilos y demas
* pages: para las UI

Y dentro de cada una de ellas podemos encontrar otras sub carpetas que tendrán archivos específicos.

### Core
1. Blocs. donde estaran todos los blocs (en este caso cubits) usados para el manejo de metodos y estados de la app
2. Models. el cual tiene todos los modelos de nuestra app
3. validators. donde se encuentran validadores para los formularios

### Data
1. Network. donde estan los archivos que realizaran los llamados a las API's
2. Repository. las clases abstractas que indican los metodos a implementar, se creo como buena practica ya declarando esta clase se podria variar entre los llamados de una API real o una FakeApi que puede utilizarse con fines de realizar pruebas

### Pages
Aqui estan las vistas o paginas de la app mas la carpeta widgets, donde tambien hay carpetas separadas por el tipo de widget a utilizar

## Screenshots
### Registro y login
![registro](https://gitlab.com/l0vecraft/prueba_wigilabs/-/raw/main/assets/register.jpeg) ![login](https://gitlab.com/l0vecraft/prueba_wigilabs/-/raw/main/assets/login.jpeg)

### Home 
![pagina de inicio](https://gitlab.com/l0vecraft/prueba_wigilabs/-/raw/main/assets/home.jpeg)
### Categories
![listado de categorias](https://gitlab.com/l0vecraft/prueba_wigilabs/-/raw/main/assets/category.jpeg)
### Song detail
![detalle de cancion](https://gitlab.com/l0vecraft/prueba_wigilabs/-/raw/main/assets/songDetail.jpeg)
### Artist detail
![detalle del artista](https://gitlab.com/l0vecraft/prueba_wigilabs/-/raw/main/assets/artistDetail.jpeg)
### Album detail
![detalle del album](https://gitlab.com/l0vecraft/prueba_wigilabs/-/raw/main/assets/albumDetail.jpeg)
## ¿Por qué utilizar Flutter?
una de las razones mas importantes de el porque utilizar esta  tecnologia y no otra es la facilidad con la que un desarrollo complejo se puede abordar, por ejemplo, al momento de implementar el inicio de sesion de Facebook por el simple echo de usar flutter y no codigo 100% nativo se ahorro un monton de lineas extras de codigo e importaciones adicionales y sin mencionar claro la facilidad de con 1 solo codigo fuente exportar una app para diversas plataformas sin que estas pierdan rendimiento.
